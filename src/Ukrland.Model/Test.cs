﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ukrland.Model
{
    public class Test
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public string Author { get; set; }
    }
}
