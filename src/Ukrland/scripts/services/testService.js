﻿(function () {
    'use strict';

    var testService = angular.module('testService', ['ngResource']);

    testService.factory('TestService', ['$resource', function ($resource) {
        return $resource('/api/test/', {}, {
            query: { method: 'GET', params: {}, isArray: true }
        });
    }]);

})();