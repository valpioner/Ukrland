﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('testController', testController);

    testController.$inject = ['$scope', 'TestService'];

    function testController($scope, TestService) {
        $scope.testList = TestService.query();
    }
})();
