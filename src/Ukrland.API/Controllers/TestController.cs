﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Ukrland.Model;

namespace Ukrland.API.Controllers
{
    public class TestController : ApiController
    {
        // Mock a data store:
        private static List<Test> _Db = new List<Test>
            {
                new Test { Id = 1, Content = "Microsoft", Author="au1" },
                new Test { Id = 2, Content = "Google", Author="au2" },
                new Test { Id = 3, Content = "Apple", Author="au3" }
            };


        public IEnumerable<Test> Get()
        {
            return _Db;
        }


        public Test Get(int id)
        {
            var company = _Db.FirstOrDefault(c => c.Id == id);
            if (company == null)
            {
                throw new HttpResponseException(
                    System.Net.HttpStatusCode.NotFound);
            }
            return company;
        }


        public IHttpActionResult Post(Test test)
        {
            if (test == null)
            {
                return BadRequest("Argument Null");
            }
            var companyExists = _Db.Any(c => c.Id == test.Id);

            if (companyExists)
            {
                return BadRequest("Exists");
            }

            _Db.Add(test);
            return Ok();
        }


        public IHttpActionResult Put(Test test)
        {
            if (test == null)
            {
                return BadRequest("Argument Null");
            }
            var existing = _Db.FirstOrDefault(c => c.Id == test.Id);

            if (existing == null)
            {
                return NotFound();
            }

            existing.Content = test.Content;
            return Ok();
        }


        public IHttpActionResult Delete(int id)
        {
            var test = _Db.FirstOrDefault(c => c.Id == id);
            if (test == null)
            {
                return NotFound();
            }
            _Db.Remove(test);
            return Ok();
        }
    }
}
