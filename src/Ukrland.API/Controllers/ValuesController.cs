﻿using System.Collections.Generic;
using Microsoft.AspNet.Mvc;
using Ukrland.Model;

namespace Ukrland.API.Controllers
{
    public class ValuesController : System.Web.Http.ApiController
    {
        // GET: api/values
        [HttpGet]
        public IEnumerable<Test> Get()
        {
            return new List<Test>
            {
                new Test { Id=1, Content="Content1", Author="Author1" },
                new Test { Id=2, Content="Content2", Author="Author2" },
                new Test { Id=3, Content="Content3", Author="Author3" }
            };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
