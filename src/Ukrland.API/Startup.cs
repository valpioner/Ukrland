﻿using Owin;
using System.Web.Http;
using Microsoft.Owin.Cors;

namespace Ukrland.API
{
    public class Startup
    {
        // This method is required by Katana:
        public void Configuration(IAppBuilder app)
        {
            var webApiConfiguration = ConfigureWebApi();
            
            app.UseCors(CorsOptions.AllowAll);            
            app.UseWebApi(webApiConfiguration);
        }

        private HttpConfiguration ConfigureWebApi()
        {
            var config = new HttpConfiguration();

            // "app.UseCors(CorsOptions.AllowAll);" should allow CORS globally
            // config.EnableCors(); 

            config.Routes.MapHttpRoute(
                "DefaultApi",
                "api/{controller}/{id}",
                new { id = RouteParameter.Optional });

            return config;
        }
    }
}
